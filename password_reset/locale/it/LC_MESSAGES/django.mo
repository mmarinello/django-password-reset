��            )   �      �  E   �  L   �  f   $  "   �     �  
   �  C   �  ^        b     h     u     �     �     �     �     �     �  3     k   ?     �     �     �           	  g     W   �  5   �  V     B  h  Y   �  `   	  �   f	  %   �	     
     
  H   &
  j   o
     �
     �
     �
     	     "     4     T     n     �  R   �  {   �  0   r  "   �     �     �     �  n     [   t  -   �  f   �                                  	                
                                                                                        An administrator on %(domain)s sent you this password recovery email. An administrator on <i>%(domain)s</i> sent you this password recovery email. An email was sent to <strong>%(email)s</strong> %(ago)s ago. Use the link in it to set a new password. Dear %(full_name)s (%(username)s), Email Greetings, Hi, <strong>%(username)s</strong>. Please choose your new password. If you don't want to reset your password, simply ignore this email and it will stay unchanged. Login New password New password (confirm) New password set Password recovery Password recovery on %(domain)s Password recovery sent Recover my password Set new password Sorry, inactive users can't recover their password. Sorry, this password reset link is invalid. You can still <a href="%(recovery_url)s">request a new one</a>. Sorry, this user doesn't exist. The two passwords didn't match. Unable to find user. Username Username or Email You -- <em>or someone pretending to be you</em> -- has requested a password reset on <i>%(domain)s</i>. You -- or someone pretending to be you -- has requested a password reset on %(domain)s. You can set your new password by following this link: Your password has successfully been reset. You can use it right now on the login page. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Un amministratore su %(domain)s le ha inviato questa mail per il recupero della password. Un amministratore su <i>%(domain)s</i> le ha inviato questa mail per il recupero della password. Una email è stata inviata a <strong>%(email)s</strong> %(ago)s fa. Utilizzi il link al suo interno per impostare una nuova password Gentile %(full_name)s (%(username)s), E-mail Cordialmente, Buongiorno, <strong>%(username)s</strong>. Scelga la Sua nuova password. Se non desidera modificare la Sua password può semplicemente ignorare questa e-mail e rimarrà invariata. Login Nuova password Nuova password (conferma) Nuova password impostata Recupero password Recupero password su %(domain)s Recupero password inviato Recupera la mia password Impostazione nuova password Siamo spiacenti ma gli utenti inattivi non possono recuperare la propria password. Siamo spiacenti ma questo link di recupero non è valido. Può sempre richiederne <a href="%(recovery_url)s">uno nuovo</a>. Siamo spiacenti ma questo utente è inesistente. Le due password non corrispondono. Impossibile trovare l'utente. Nome utente Nome utente o e-mail Lei -- <em>o qualcuno che pretende di essere lei</em> -- ha richiesto un cambio password su <i>%(domain)s</i>. Lei - o qualcuno che pretende di essere lei - ha richiesto un cambio password su %(domain)s Può impostare la Sua password a questo link: La Sua password è stata reimpostata con successo. Può utilizzarla da subito nella pagina di accesso. 